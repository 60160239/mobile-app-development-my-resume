import 'package:flutter/material.dart';

Column _buildButtonCol(Color color, String icon, String label) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Image.asset(
        icon,
        height: 50,
        width: 50,
      ),
      Container(
          child: Text(
        label,
        style:
            TextStyle(fontSize: 16, fontWeight: FontWeight.w400, color: color),
      )),
    ],
  );
}

TextStyle stylenormal = TextStyle(
            fontSize: 16,
            height: 1.5
          );
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget titleSect = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'Somewhere lake Campground',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                Text(
                  'Somewhere, Switzerland',
                  style: TextStyle(color: Colors.grey[500]),
                )
              ],
            ),
          ),
          Icon(
            Icons.star,
            color: Colors.red[500],
          ),
          Text('41')
        ],
      ),
    );
    Widget skillSect = Container(
      child: Column(
        children: [
          Text('Skill'),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _buildButtonCol(color, 'images/unityicon.png', 'Unity'),
              _buildButtonCol(color, 'images/cplusicon.png', 'C++'),
              _buildButtonCol(color, 'images/jsicon.jpg', 'Javascript'),
              _buildButtonCol(color, 'images/mysqlicon.png', 'MySQL'),
            ],
          )
        ],
      ),
    );
    Widget moreinfotextSect = Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.all(32),
        child: Text(
          'ประวัติการศึกษา\n'
          'มหาวิทยาลัยบูรพา Burapha University\n'
          'from 2018 to present\n'
          'โปรเจค Personal & Group Project\n'
          'Unity game project : Celurean Cypher',
          softWrap: true,
          style: stylenormal
        ));
    Widget maininfotextSect = Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.all(32),
        child: Text(
          'ที่อยู่ Address\n'
          '35 M.3 Donhualoi Muang Chonburi, Chon Buri 20000\n'
          'ช่องทางการติดต่อ Contact\n'
          'zerochannal@gmail.com\n'
          '097-271-6319',
          softWrap: true,
          style: stylenormal,
        ));
    return MaterialApp(
      title: 'My Resume',
      home: Scaffold(
        appBar: AppBar(
            title: const Text(
          'My Resume',
          textAlign: TextAlign.center,
        )),
        body: ListView(
          children: [
            Container(
              child: Column(
                children: [
                  Row(
                    children: [
                      Image.asset('images/getstudentimage.jpg',height: 200,width: 200,),
                      SizedBox(width: 20,),
                      Expanded(
                          child: Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Expanded(
                                  child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('นาย ชนะชนม์ ด่านปรีดา',style: stylenormal,),
                                  Text('Mister Chanachon Danpreeda',style: stylenormal,)
                                ],
                              )),
                              Container(
                                padding: EdgeInsets.only(left: 20,right: 20),
                                child: Column(
                                children: [Text('อายุ',style: stylenormal,), Text('23',style: stylenormal,)],
                                )
                              ),
                              Column(
                                children: [Text('เพศ',style: stylenormal,), Text('ชาย',style: stylenormal,)],
                              )
                            ],
                          ),
                          maininfotextSect
                        ],
                      ))
                    ],
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Column(
                      children: [moreinfotextSect, skillSect],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
